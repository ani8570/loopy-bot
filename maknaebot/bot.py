from datetime import datetime
from requests import request
from slack_sdk import WebClient
from slack_sdk.errors import SlackApiError
from maknaebot.utils import date_fun
from maknaebot.utils.lunch_menu import Lunch_menu
from maknaebot.utils.logger import maknaelogger
import time
import os
import requests
from PIL import Image

LAB = 'C02GD640CLU'
WAMENTLE = 'C04KGJ1BU8M'
class Bot():
    """
    슬랙 API 핸들러
    """
    def __init__(self, id, password, token):
        # 슬랙 클라이언트 인스턴스 생성
        self.client = WebClient(token)
        self.id = id
        self.password = password
        
    def post_message(self, channel_id, text):
        """
        슬랙 채널 내 메세지의 Thread에 글 달기
        """
        # chat_postMessage() 메서드 호출
        try:
            # Call the chat.postMessage method using the WebClient
            result = self.client.chat_postMessage(
                channel=channel_id, 
                text=text
                )
            maknaelogger.debug(result)
            return result
        except SlackApiError as e:
            maknaelogger.error("Error posting message: ")
            maknaelogger.error(e)
            
    def post_img(self, channel_id, today, image_url, filename):
        """
        슬랙 채널 내 메세지의 Thread에 글 달기
        """
        # chat_postMessage() 메서드 호출
        try:
            # Call the chat.postMessage method using the WebClient
            img_folder_path = os.path.join(os.getcwd(), 'menu', today)
            try:
                if not os.path.exists(img_folder_path):
                    os.makedirs(img_folder_path)
            except OSError:
                maknaelogger.error("Error: Cannot create the directory {}".format(img_folder_path))
            
            maknaelogger.info('post_img 실행 : ' + today + ' ' + image_url)
            file = Image.open(requests.get(image_url, stream=True).raw)
            filename = os.path.join(img_folder_path, filename + '.png')
            file.save(filename)
            result = self.client.files_upload(
                channels=channel_id, 
                file= filename
                )
            maknaelogger.debug(result)
            return result
        except SlackApiError as e:
            maknaelogger.error("Error posting message: ")
            maknaelogger.error(e)
            
    def alert_welstory_menu(self, channel_id):
        maknaelogger.info('welstory 알람 실행')

        if date_fun.is_holiday() :
            maknaelogger.info("오늘은 공휴일입니다.")
            return "오늘은 공휴일입니다."
        if date_fun.get_today_weekday() == 5 or date_fun.get_today_weekday() == 6:
            maknaelogger.info("오늘은 주말입니다.")
            return "오늘은 주말입니다."

        today = date_fun.get_today()
        menus = Lunch_menu()
        time.sleep(3)
        menu_url_list = menus.get_welstory_menu(today, self.id, self.password)
        if menu_url_list :
            menu_list = [i[0] for i in menu_url_list]
            url_list = [i[1] for i in menu_url_list]
            text = "오늘 점심 웰스토리 메뉴는 [" + ", ".join(menu_list) + "] 입니다."
            maknaelogger.info(text)
            self.post_message(channel_id, text)
            for i in range(len(url_list)):
                self.post_img(channel_id, today, url_list[i], 'menu' + str(i))
            return text
        return ""
    def alert_hancom_menu(self, channel_id):
        maknaelogger.info('hancom 알람 실행')
        today = date_fun.get_today()
        if date_fun.is_holiday() :
            maknaelogger.info("오늘은 공휴일입니다.")
            return "오늘은 공휴일입니다."
        if date_fun.get_today_weekday() == 5 or date_fun.get_today_weekday() == 6:
            maknaelogger.info("오늘은 주말입니다.")
            return "오늘은 주말입니다."
        menus = Lunch_menu()
        time.sleep(3)
        menu_url_list = menus.get_hancom_menu(today, self.id, self.password)
        maknaelogger.info(str(menu_url_list[0] + ' ' + str(menu_url_list[1])))
        if menu_url_list:
            text = "오늘 점심 한컴 메뉴는 \n"+ menu_url_list[0]+"\n 입니다."
            maknaelogger.info(text)
            self.post_message(channel_id, text)
            self.post_img(channel_id, today, menu_url_list[1], 'Hancom')
            return text
        return ""

    def alert_pms(self, channel_id):
        maknaelogger.info('pms 알람 실행')
        today = date_fun.get_today()
        last_day = date_fun.get_last_day_in_month()
        maknaelogger.info("월말일 : " + last_day)
        if today != last_day:
            maknaelogger.info("오늘은 월말이 아닙니다.")
            return "오늘은 월말이 아닙니다."
        text = "오늘은 월말입니다. PMS 작성해주세요."
        maknaelogger.info(text)
        self.post_message(channel_id, text)
        return text

    def alert_every_hour(self) :
        text = datetime.today().strftime("%Y%m%d")
        maknaelogger.info(text)
        return text