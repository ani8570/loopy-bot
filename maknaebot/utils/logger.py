from datetime import datetime
import logging
import logging.handlers

maknaelogger = logging.getLogger(__name__)
formatter = logging.Formatter(  '%(asctime)s - %(levelname)s -  %(message)s'  )

# handler 생성 (stream, file)
streamHandler = logging.StreamHandler()
streamHandler.setFormatter(formatter)

timedfilehandler = logging.handlers.TimedRotatingFileHandler(filename='./log/bot.log', when='midnight', interval=1, encoding='utf-8')
timedfilehandler.setFormatter(formatter)

# logger instance에 handler 설정
maknaelogger.addHandler(streamHandler)
maknaelogger.addHandler(timedfilehandler)

maknaelogger.setLevel(level=logging.DEBUG)
