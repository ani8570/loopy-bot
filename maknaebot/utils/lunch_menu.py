import random
import time
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from maknaebot.utils.logger import maknaelogger

class Lunch_menu() :
    def __init__(self) :
        def set_chrome_driver():
            service = Service()
            chrome_options = webdriver.ChromeOptions()
            chrome_options.add_argument('--headless')
            chrome_options.add_argument('--no-sandbox')
            chrome_options.add_argument('--disable-gpu')
            chrome_options.add_argument('lang=ko_KR')
            driver = webdriver.Chrome(service=service, options=chrome_options)
            return driver
            
        time.sleep(random.uniform(1,3)) 
        self.driver = set_chrome_driver()
    
        
    def get_welstory_menu(self, today, id , password) :
        def set_welstory_login():
            self.driver.get("http://112.106.28.112/#/login/username")
            time.sleep(random.uniform(3,5)) 
            self.driver.find_element(By.ID, 'wrtId').send_keys(id)
            self.driver.find_element(By.ID, "wrtPw").send_keys(password + Keys.ENTER)
            time.sleep(random.uniform(1,2)) 
        def get_menus() :
            def get_menu() :
                self.driver.get('http://112.106.28.112/#/meal/detail?menuDt=' + today + '&hallNo=E1B7&menuCourseType='+ menuCourseType +'&menuMealType=2&restaurantCode=REST000184')
                maknaelogger.info('http://112.106.28.112/#/meal/detail?menuDt=' + today + '&hallNo=E1B7&menuCourseType='+ menuCourseType +'&menuMealType=2&restaurantCode=REST000184')
                time.sleep(random.uniform(5,10)) 
                self.driver.refresh()
                time.sleep(random.uniform(5,10)) 
                try :
                    name_and_img = []
                    menu_name = self.driver.find_element(By.CLASS_NAME, 'tit-big').text
                    
                    if menu_name:
                        name_and_img.append(menu_name)
                    img_url = self.driver.find_element(By.CLASS_NAME, 'thumb').get_attribute('style').split(sep='"')
                    
                    if name_and_img :
                        name_and_img.append(img_url[1])
                    
                    maknaelogger.info(str(name_and_img))
                    return name_and_img
                except Exception as e:
                    maknaelogger.exception(e)
                    return []
            
            menus = []
            for menuCourseType in ['AA','EE','FF', 'HH'] :
                menu = get_menu()
                if menu :
                    menus.append(get_menu())
            return menus
        
        set_welstory_login()    
        menus = get_menus()
        self.close_driver()
        return menus
    
    def get_hancom_menu(self, today, id , password) :
        try :
            self.driver.get('https://www.instagram.com')
            time.sleep(random.uniform(10,12)) 
            id_path = '//*[@id="loginForm"]/div/div[1]/div/label'
            password_path = '//*[@id="loginForm"]/div/div[2]/div/label'
            self.driver.find_element(By.XPATH, id_path).send_keys('wisenut' + id)
            self.driver.find_element(By.XPATH, password_path).send_keys(password + Keys.ENTER)
            time.sleep(random.uniform(10,12)) 
            self.driver.find_elements(By.TAG_NAME, 'button')[1].click()
            time.sleep(random.uniform(10,12)) 
            cnt = 1
            result = []

            while(True) :
                self.driver.get('https://www.instagram.com/delightfood_1/')
                time.sleep(random.uniform(10,12)) 
                url_list = self.driver.find_elements(By.TAG_NAME, 'a')
                
                url = ''
                for i in url_list :
                    url = i.get_attribute("href")
                    if url.split('.com')[1].startswith('/p') :
                        break

                self.driver.get(url)
                time.sleep(random.uniform(10,12)) 
                
                upload_time = self.driver.find_elements(By.TAG_NAME, 'time')[0].get_attribute('datetime').replace('-','')
                if upload_time[:len(today)] < today :
                    maknaelogger.info(str(cnt) + '번째 실패')
                    cnt += 1
                    if cnt == 5 :
                        break
                    time.sleep(5 * 60)
                else :
                    menus = self.driver.find_elements(By.TAG_NAME,'h1')
                    for i in menus :
                        menu = i.text
                        if len(menu) > len('delightfood_1') and menu.startswith('💛'):
                            img_urls = self.driver.find_elements(By.TAG_NAME, 'img')
                            url = ''
                            for i in img_urls :
                                url = i.get_attribute('src')
                                if url.startswith('https://scontent-ssn1-1.cdninstagram.com'):
                                    break
                            result.append(menu)
                            result.append(url)
                            break
                    break
            self.close_driver()
            return result
        
        except Exception as e:
            maknaelogger.exception(e)
            self.close_driver()
            return []

    def close_driver(self) :
        self.driver.quit()