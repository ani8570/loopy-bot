from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
from datetime import datetime
import requests
import json
import pandas as pd
from pandas import json_normalize

def get_today():
    current_date = datetime.today().strftime("%Y%m%d")
    return current_date

def get_today_weekday():
    return datetime.date(datetime.today()).weekday()

def get_timestamp() :
    day = datetime.date.today()
    scheduled_time = datetime.time(hour=22, minute=53)
    schedule_timestamp = datetime.datetime.combine(day, scheduled_time).strftime('%S')
    return schedule_timestamp


def get_last_day_in_month() :
    first_day_in_month = datetime.date(datetime.today() + relativedelta(months=1)).replace(day=1)
    last_day_in_month = first_day_in_month - timedelta(days=1)
    # 5,6 -> 토요일, 일요일
    while(True) :
        if last_day_in_month.weekday() == 5 or last_day_in_month.weekday() == 6 :
            last_day_in_month = last_day_in_month - timedelta(days=1)
        elif is_holiday() :
            last_day_in_month = last_day_in_month - timedelta(days=1)
        else :
            break
        
    return last_day_in_month.strftime("%Y%m%d")

def is_holiday() :
    today = datetime.today().strftime('%Y%m%d')
    today_year = datetime.today().year
        
    key = 'u2hC3TW2YT%2FBkHZ20bQjcuIbscn2PwwZjxr8%2F%2BLiopF2sI4SaO0evJjQu3A4hgT0gPZK%2Bmwy6KWos6i7e4UwyA%3D%3D'
    url = 'http://apis.data.go.kr/B090041/openapi/service/SpcdeInfoService/getRestDeInfo?_type=json&numOfRows=50&solYear=' + str(today_year) + '&ServiceKey=' + str(key)
    response = requests.get(url)
    if response.status_code == 200:
        json_ob = json.loads(response.text)
        holidays_data = json_ob['response']['body']['items']['item']
        dataframe = json_normalize(holidays_data)
    dateName = dataframe.loc[dataframe['locdate'] == int(today), 'dateName']
    if dateName.values:
        return True
    return False

def main():
    print(get_today())
    print(get_last_day_in_month())

if __name__ == '__main__':
    main()
    