import os
from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.triggers.cron import CronTrigger
from dotenv import load_dotenv
from flask import Flask
from pytz import utc, timezone
from datetime import date

from process_similar import get_nearest

from maknaebot.bot import Bot
from maknaebot.utils.logger import maknaelogger
from slackeventsapi import SlackEventAdapter
from pathlib import Path
import os, pickle, word2vec

KST = timezone('Asia/Seoul')

NUM_SECRETS = 4650

scheduler = BackgroundScheduler()
scheduler.start()

env_path = Path('.') / '.env'
load_dotenv(dotenv_path=env_path)

app = Flask(__name__)
app.config['JSON_AS_ASCII'] = False
app.LAB=os.environ['LAB']
# app.WAMENTLE=os.environ['WAMENTLE']
app.bot = Bot(id=os.environ['ID'], password=os.environ['PASSWORD'], token=os.environ['TOKEN'])

# maknaelogger.info("loading valid nearest")
# with open('data/valid_nearest.dat', 'rb') as f:
#     valid_nearest_words, valid_nearest_vecs = pickle.load(f)
# with open('data/secrets.txt', 'r', encoding='utf-8') as f:
#     secrets = [l.strip() for l in f.readlines()]
# maknaelogger.info("initializing nearest words for solutions")
# app.secrets = dict()
# app.nearests = dict()
# app.DAY = (date.today() - date(2022,12,25)).days
# app.DAY += 1
# current_puzzle = (app.DAY) % NUM_SECRETS
# for offset in range(-2, 2):
#     puzzle_number = (current_puzzle + offset) % NUM_SECRETS
#     secret_word = secrets[puzzle_number]
#     app.secrets[puzzle_number] = secret_word
#     app.nearests[puzzle_number] = get_nearest(puzzle_number, secret_word, valid_nearest_words, valid_nearest_vecs)
# app.word = ""
# app.sim = 0
# app.rank = ""
# app.user_count = {}
# app.word_dic = {}
# slack_event_adapter = SlackEventAdapter(os.environ['SIGNING_SECRET'], '/slack/events', app)

# @scheduler.scheduled_job(trigger=CronTrigger(hour=1, minute=0, second=0, timezone=KST))
# def update_nearest():
#     app.bot.post_message(app.WAMENTLE, "새로운 대회 시작")
#
#     app.user_count = {}
#     app.word_dic = {}
#     app.DAY += 1
#     next_puzzle = (app.DAY) % NUM_SECRETS
#     next_word = secrets[next_puzzle]
#     to_delete = (next_puzzle - 4) % NUM_SECRETS
#     if to_delete in app.secrets:
#         del app.secrets[to_delete]
#     if to_delete in app.nearests:
#         del app.nearests[to_delete]
#     app.secrets[next_puzzle] = next_word
#     app.nearests[next_puzzle] = get_nearest(next_puzzle, next_word, valid_nearest_words, valid_nearest_vecs)

@scheduler.scheduled_job(trigger=CronTrigger(minute=0, second=0, timezone=KST))
@app.route('/alert/time')
def alert_every_hour():
    return app.bot.alert_every_hour()


@scheduler.scheduled_job(
    trigger=CronTrigger(hour=9, minute=5, second=0, timezone=KST))
@app.route('/alert/pms')
def alert_pms():
    return app.bot.alert_pms(app.LAB)


@scheduler.scheduled_job(
    trigger=CronTrigger(hour=9, minute=35, second=0, timezone=KST))
@app.route('/alert/welstory')
def alert_welstory_menu():
    return app.bot.alert_welstory_menu(app.LAB)


@scheduler.scheduled_job(
    trigger=CronTrigger(hour=11, minute=15, second=0, timezone=KST))
@app.route('/alert/hancom')
def alert_hancom_menu():
    return app.bot.alert_hancom_menu(app.LAB)


@app.route('/')
def get_index():
    return "loopy~"


# @slack_event_adapter.on('message')
# def message(payload):
#     event = payload.get('event', {})
#     channel_id = event.get('channel')
#     if channel_id != app.WAMENTLE:
#         return
#
#     user_id = event.get('user')
#     if user_id == os.environ['BOT_ID']:
#         return
#
#     response = app.bot.client.users_info(token=os.environ['TOKEN'],user=user_id)
#     user_name = response['user']['real_name']
#     word = event.get('text')
#     get_guess(word, user_name)


# def get_guess(word, user_name):
#     maknaelogger.info("keyword: "+ word + ", 이름: " + user_name)
#     if not user_name in app.user_count :
#         app.user_count[user_name] = 0
#     app.user_count[user_name] += 1
#
#     if app.secrets[app.DAY].lower() == word.lower():
#         word = app.secrets[app.DAY]
#
#     rtn = {"guess": word}
#     # check most similar
#     if app.DAY in app.nearests and word in app.nearests[app.DAY]:
#         rtn["sim"] = round(app.nearests[app.DAY][word][1] * 100 , 2)
#         rtn["rank"] = app.nearests[app.DAY][word][0]
#
#         if rtn["sim"] == 100 :
#             app.bot.post_message(app.WAMENTLE,  "정답: " + rtn["guess"] + ", 정답자 : " + user_name + ", 시도회수 : " + str(app.user_count[user_name] + 1))
#             update_nearest()
#             return
#     else:
#         rtn["rank"] = "1000위 이상"
#         try:
#             rtn["sim"] = round(word2vec.similarity(app.secrets[app.DAY], word) * 100 , 2)
#         except KeyError:
#             rtn["sim"] = -100
#
#     app.bot.post_message(app.WAMENTLE, "keyword: "+ rtn["guess"] + ", 유사도 : " + str(rtn["sim"]) + ", 순위 : " + str(rtn["rank"]))

if __name__ == '__main__':
    maknaelogger.info("start!")
    app.run(host='0.0.0.0', port=12345)
    # app.run()